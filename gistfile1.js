  passport.use(new LocalStrategy(
    function(username, password, done) {
      
      collections.UserCollection.forge()
      .query(function (qb) {
        qb.where('username', '=', username.toLowerCase());
      })
      .fetchOne()
      .then(function (user) {
        if (user == null) {
          return done(null, false, { message: 'Incorrect credentials.' });
        };
        
        //var hashedPassword = bcrypt.hashSync(password, salt)
        console.log(user.attributes.password);
        
        return bcrypt.compareAsync(password, user.attributes.password);
      })
      .then(function(res) {
        return done(null, user, { message: 'You are logged in.' });
      })
      .catch(function(err) {
        return done(null, false, { message: 'Incorrect password.' });
      });
    })